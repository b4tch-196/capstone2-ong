// DEPENDENCIES
const jwt = require("jsonwebtoken");
const secret = "masteryAPI";


module.exports.createAccessToken = (userDetails) => {

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	// console.log(data);
	return jwt.sign(data,secret,{});
}

// REFULAR USER VERIFICATION
module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization

	if(typeof token === "undefined"){
		// typeof result must be a string
		return res.send({auth: "Failed. No Token."})

	} else {

		token = token.slice(7);
		// console.log(token);

		jwt.verify(token,secret,function(err,decodedToken){
		
			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				});

			} else {
				req.user = decodedToken;
				// console.log(decodedToken)
				next();
			}
		})
	}
}

// ADMIN VERIFICATION
module.exports.verifyAdmin = (req,res,next) => {

		//verifyAdmin must come after verify to have access to req.user
		// console.log(req.user);

		// check if user is an admin or not
		// if user is an admin run next() method
		// Else return a message to the client
		if(req.user.isAdmin){
			next();

		} else {
			return res.send({
				auth:"Failed",
				message: "Action Forbidden"
			})
		}
	}