// DEPENDENCIES
const Course = require("../models/Course");

// IMAGE UPLOAD DEPENDENCIES
// const multer = require('multer');
// const path = require('path');

// UPLOAD IMAGE

// const storage = multer.diskStorage({
// 	destination: (req,file,cb) => {
// 		cb(null, 'images')
// 	},
// 	filename: (req,file,cb) => {
// 		cb(null, Date.now() + path.extname(file.originalname))
// 	}
// })

// const upload = multer ({
// 	storage: storage,
// 	limits: {fileSize: '8000000'},
// 	fileFilter: (req,file,cb) => {
// 		const fileTypes = /jpeg|jpg|png|gif/
// 		const mimeType = fileTypes.test(file.mimetype)
// 		const extname = fileTypes.test(path.extname(file.originalname))

// 		if(mimeType && extname){
// 			return cb(null, true)
// 		}
// 		cb('Give proper files formate to upload')
// 	}
// }).single('image')

// module.exports = {
// 	upload
// }


// ADD COURSE
module.exports.addCourse = (req,res) =>{

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		image: req.body.image
	})

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// CHECK EXISTING COURSE NAME
module.exports.checkCourseName = (req,res) => {

	Course.findOne({name:req.body.name})
	.then(result => {

		if(result === null){
			return res.send(false)

		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error));
}


// GET SINGLE COURSE
module.exports.getCourse = (req,res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// GET ACTIVE COURSES
module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// GET ALL COURSES
module.exports.getAllCourses = (req,res) =>{

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// UPDATE A COURSE
module.exports.updateCourse = (req,res) => {

	let update = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		image: req.body.image
	};

	Course.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// ARCHIVE/DISABLE COURSE
module.exports.disableCourse = (req,res) => {

	let update = {
		isActive: false
	};

	Course.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// ACTIVATE COURSE
module.exports.activateCourse = (req,res) => {

	let update = {
		isActive: true
	};

	Course.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// DELETE COURSE
module.exports.deleteCourse = (req,res) => {

	Course.findByIdAndDelete(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

// FIND COURSES BY NAME
module.exports.findCoursesByName = (req,res) => {

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send(false);

		} else {
			return res.send(result)
		}
	});
};