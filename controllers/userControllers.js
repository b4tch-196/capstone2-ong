// DEPENDENCIES
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// REGISTER USER
module.exports.registerUser = (req,res) =>{

	const hashedPw = bcrypt.hashSync(req.body.password, 10)

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		email: req.body.email,
		password: hashedPw
	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// LOGIN USER
module.exports.loginUser = (req,res) =>{
	// mongodb: db.user.findOne({email:})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send(false);
	
		} else {
			// bcrypt.compareSync(<inputString>,<hashedString>)
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});

			}else {
				return res.send(false);
			}
		}
	})
	.catch(error => res.send(error));
};


// CHECK EMAIL EXISTENCE
module.exports.checkEmail = (req,res) => {

	User.findOne({email:req.body.email})
	.then(result => {

		if (result === null){
			return res.send(false)

		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error));
};


// GET USER DETAILS
module.exports.getUserDetails = (req,res) =>{

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


// GET ALL USER DETAILS
module.exports.getAllUserDetails = (req,res) =>{

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


// UPDATE USER DETAILS
module.exports.updateUserDetails = (req,res) => {

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	};

	User.findByIdAndUpdate(req.user.id, updates, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};


// SET ADMIN
module.exports.setAdmin = (req,res) => {

	let update = {
		isAdmin: true
	};

	User.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// UN-SET ADMIN
module.exports.unsetAdmin = (req,res) => {

	let update = {
		isAdmin: false
	};

	User.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


// ENROLL USER
module.exports.enroll = async (req,res) => {

	// prevent admin from enrolling
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"})
	}

	let isUserUpdated = await User.findById(req.user.id)
	.then(user =>{

		// create an object which will contain the course id of the newEnrollment of a user
		let newEnrollment = {
			courseId: req.body.courseId
		}
		

		// push the newEnrollment into the enrollments subdocument array of the user
		user.enrollments.push(newEnrollment)

		return user.save()
		.then(user => true)
		.catch(err => err.message)
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}

	let isCourseUpdated = await Course.findById(req.body.courseId)
	.then(course => {

		// create an object which will contain the user id of the enrollee of a course
		let enrollee = {
			userId: req.user.id
		}

		// push the enrollee into the enrollees subdocument array of the course:
		course.enrollees.push(enrollee);

		return course.save()
		.then(course => true)
		.catch(err => err.message);
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send(true)
	}
};


// GET ENROLLMENTS
module.exports.getEnrollments = (req,res) => {
	
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(error => res.send(error));
};