// DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// SERVER
const app = express();
const port = process.env.PORT || 4000;

// DATABASE CONNECTION (MONGOOSE)
mongoose.connect("mongodb+srv://admin:admin123@cluster0.ycpxqir.mongodb.net/arcanumAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// CONSOLE MESSAGES
let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Connected to MongoDB."))


// MIDDLEWARES
app.use(express.json());
app.use(cors());
// MIDDLEWARE FOR NESTED OBJECTS 'POST'
app.use(express.urlencoded({extended: true}));


// USER ROUTES
const userRoutes = require('./routes/userRoutes')
app.use('/users',userRoutes);


// COURSE ROUTES
const courseRoutes = require('./routes/courseRoutes');
app.use('/courses',courseRoutes);


// PORT LISTENER
app.listen(port,() => console.log(`Server is running at port ${port}`));