// DEPENDENCIES
const express = require("express");
const router = express.Router();

// IMPORTED MODULES
const courseControllers = require("../controllers/courseControllers")
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// ADD COURSE
router.post('/',verify,verifyAdmin,courseControllers.addCourse);


// CHECK EXISTING COURSE NAME
router.post('/checkCourseName',courseControllers.checkCourseName);


// GET SINGLE COURSE
router.get('/getCourse/:id',courseControllers.getCourse);


// GET ACTIVE COURSES
router.get('/activeCourses',courseControllers.getActiveCourses);


// GET ALL COURSES
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses)


// UPDATE COURSE INFORMATION
router.put('/updateCourse/:id',verify,verifyAdmin,courseControllers.updateCourse);


// ARCHIVE/DISABLE COURSE
router.put('/disableCourse/:id',verify,verifyAdmin,courseControllers.disableCourse);


// ACTIVATE COURSE
router.put('/activateCourse/:id',verify,verifyAdmin,courseControllers.activateCourse);


// DELETE COURSE
router.delete('/:id',verify,verifyAdmin,courseControllers.deleteCourse);


// FIND COURSES BY NAME
router.post('/findCoursesByName',courseControllers.findCoursesByName);


module.exports = router;