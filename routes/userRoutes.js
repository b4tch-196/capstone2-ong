// DEPENDENCIES
const express = require("express");
const router = express.Router();

// IMPORTED MODULES
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// destructure auth to get only our methods and save it in variables:
const { verify, verifyAdmin } = auth;

// ROUTES
//method("/endpoint",handlerFunction)
// verify() - verify user logged in with token


// REGISTER USER
router.post('/',userControllers.registerUser);


// LOGIN USER
router.post('/login',userControllers.loginUser);


// CHECK IF USER EMAIL EXISTS
router.post('/checkEmail',userControllers.checkEmail);


// GET USER DETAILS
router.get('/details', verify, userControllers.getUserDetails);


// GET ALL USER DETAILS
router.get('/', verify, userControllers.getAllUserDetails);


// UPDATE USER DETAILS
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);


// SET ADMIN
router.put('/setAdmin/:id', verify, verifyAdmin, userControllers.setAdmin);


// UN-SET ADMIN
router.put('/unsetAdmin/:id', verify, verifyAdmin, userControllers.unsetAdmin);


// ENROLL USER
// courseId will come from the req.body
// userId will come from the req.user
router.post('/enroll', verify, userControllers.enroll);


// GET USER ENROLLMENTS
router.get('/enroll', verify, userControllers.getEnrollments);


module.exports = router;